<?php declare(strict_types=1);

namespace Tests\Samples;

use IoC\Contracts\Resolver;
use IoC\Contracts\Container;
use Tests\Samples\DependentThing;

use Tests\Samples\Thing;

class ExampleResolver implements Resolver
{
    public function resolve(Container $container) : object
    {
        return new DependentThing(
            $container->resolve('thing'),
            $container->resolve('thing')
        );
    }
}
