<?php declare(strict_types=1);

namespace Tests\Samples;

class DependentThing
{
    public $thing1;
    public $thing2;
    
    public function __construct(Thing $thing1, Thing $thing2)
    {
        $this->thing1 = $thing1;
        $this->thing2 = $thing2;
    }
}
