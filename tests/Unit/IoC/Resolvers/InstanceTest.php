<?php declare(strict_types=1);

namespace Tests\Unit\IoC\Resolvers;

use IoC\Container;
use Tests\Samples\Thing;
use IoC\Resolvers\Instance;
use PHPUnit\Framework\TestCase;

class InstanceTest extends TestCase
{
    public function testConstructs() : void
    {
        $instance = new Instance(new Thing);

        $this->assertNotNull($instance);
    }

    public function testResolves() : void
    {
        $thing1 = new Thing;
        $thing2 = new Thing;

        $instance1 = new Instance($thing1);
        $instance2 = new Instance($thing2);

        $container = new Container;

        $this->assertSame($thing1, $instance1->resolve($container));
        $this->assertSame($thing2, $instance2->resolve($container));
    }
}
