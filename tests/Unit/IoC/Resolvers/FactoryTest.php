<?php declare(strict_types=1);

namespace Tests\Unit\Ioc\Resolvers;

use IoC\Container;
use Tests\Samples\Thing;
use IoC\Resolvers\Factory;
use PHPUnit\Framework\TestCase;
use Tests\Samples\DependentThing;

class FactoryTest extends TestCase
{
    public function testConstructs() : void
    {
        $resolver = new Factory('abc', []);

        $this->assertNotNull($resolver);
    }

    public function testResolvesSimpleClass() : void
    {
        $resolver = new Factory(Thing::class, []);
        $container = new Container;

        $thing = $resolver->resolve($container);
        
        $this->assertNotNull($thing);
        $this->assertInstanceOf(Thing::class, $thing);
    }

    public function testResolvesToDifferenceInstances() : void
    {
        $resolver = new Factory(Thing::class, []);
        $container = new Container;

        $thing1 = $resolver->resolve($container);
        $thing2 = $resolver->resolve($container);
        
        $this->assertNotSame($thing1, $thing2);
    }

    public function testResolvesDependentClass() : void
    {
        $resolver = new Factory(
            DependentThing::class,
            ['thing', 'thing']
        );
        
        $container = new Container;
        $container->register('thing', new Factory(Thing::class));
        $depThing = $resolver->resolve($container);
        
        $this->assertNotNull($depThing);
        $this->assertInstanceOf(DependentThing::class, $depThing);
        $this->assertInstanceOf(Thing::class, $depThing->thing1);
        $this->assertInstanceOf(Thing::class, $depThing->thing2);
    }
}
