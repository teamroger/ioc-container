<?php declare(strict_types=1);

namespace Tests\Unit\IoC\Resolvers;

use IoC\Container;
use Tests\Samples\Thing;
use IoC\Resolvers\Factory;
use IoC\Resolvers\Singleton;
use PHPUnit\Framework\TestCase;

class SingletonTest extends TestCase
{
    public function testConstructs() : void
    {
        $s = new Singleton(new Factory(Thing::class));

        $this->assertNotNull($s);
    }

    public function testResolvesSingletonInstance() : void
    {
        $container = new Container;
        $s = new Singleton(new Factory(Thing::class));

        $thing1 = $s->resolve($container);
        $thing2 = $s->resolve($container);

        $this->assertSame($thing1, $thing2);
    }
}
