<?php declare(strict_types=1);

namespace Tests\Unit\IoC;

use IoC\Container;
use Tests\Samples\Thing;
use IoC\Resolvers\Factory;
use IoC\Resolvers\Instance;
use IoC\Resolvers\Singleton;
use PHPUnit\Framework\TestCase;
use Tests\Samples\DependentThing;
use Tests\Samples\ExampleResolver;

class ContainerTest extends TestCase
{
    private $container;

    public function setUp() : void
    {
        $this->container = new Container;
    }

    public function testConstructsSuccessfully() : void
    {
        $this->assertNotNull($this->container);
    }

    public function testCanRegisterAndResolveValues() : void
    {
        $thing1 = new Thing;
        $thing2 = new Thing;

        $this->container
            ->register('thing1', new Instance($thing1))
            ->register('thing2', new Instance($thing2));

        $this->assertSame($thing1, $this->container->resolve('thing1'));
        $this->assertSame($thing2, $this->container->resolve('thing2'));
    }

    public function testCanRegisterAndResolveClasses() : void
    {
        $this->container->register('thing', new Factory(Thing::class));

        $t1 = $this->container->resolve('thing');
        $t2 = $this->container->resolve('thing');

        $this->assertNotSame($t1, $t2);
    }

    public function testCanRegisterAndResolveClassWithDependencies() : void
    {
        $this->container
            ->register('thing', new Factory(Thing::class))
            ->register(
                'dependentThing',
                new Factory(DependentThing::class, ['thing', 'thing'])
            );

        $dependentThing = $this->container->resolve('dependentThing');

        $this->assertNotNull($dependentThing);
        $this->assertNotSame($dependentThing->thing1, $dependentThing->thing2);
    }

    public function testCanRegisterAndResolveSingleton() : void
    {
        $this->container->register(
            'thing',
            new Singleton(new Factory(Thing::class))
        );

        $t1 = $this->container->resolve('thing');
        $t2 = $this->container->resolve('thing');

        $this->assertSame($t1, $t2);
    }

    public function testCanRegisterAndResolveClassWithSingletonDependencies() : void
    {
        $this->container
            ->register(
                'thing',
                new Singleton(new Factory(Thing::class))
            )
            ->register(
                'dependentThing',
                new Factory(
                    DependentThing::class,
                    ['thing', 'thing']
                )
            );

        $dependentThing = $this->container->resolve('dependentThing');

        $this->assertNotNull($dependentThing);
        $this->assertSame($dependentThing->thing1, $dependentThing->thing2);
    }

    public function testCanRegisterAndResolveWithCustomResolver() : void
    {
        $this->container
            ->register('thing', new Factory(Thing::class))
            ->register('customThing', new ExampleResolver);
        
        $customThing1 = $this->container->resolve('customThing');
        $customThing2 = $this->container->resolve('customThing');

        $this->assertNotNull($customThing1);
        $this->assertNotNull($customThing2);

        $this->assertNotSame($customThing1, $customThing2);
        
        $this->assertInstanceOf(
            Thing::class,
            $customThing1->thing1
        );

        $this->assertInstanceOf(
            Thing::class,
            $customThing1->thing2
        );

        $this->assertInstanceOf(
            Thing::class,
            $customThing2->thing1
        );

        $this->assertInstanceOf(
            Thing::class,
            $customThing2->thing2
        );
    }

    public function testCanRegisterAndResolveOnceWithCustomResolver() : void
    {
        $this->container
            ->register('thing', new Factory(Thing::class))
            ->register('customThing', new Singleton(new ExampleResolver));
        
        $customThing1 = $this->container->resolve('customThing');
        $customThing2 = $this->container->resolve('customThing');

        $this->assertNotNull($customThing1);
        $this->assertNotNull($customThing2);

        $this->assertSame($customThing1, $customThing2);
        
        $this->assertInstanceOf(
            Thing::class,
            $customThing1->thing1
        );

        $this->assertInstanceOf(
            Thing::class,
            $customThing1->thing2
        );

        $this->assertInstanceOf(
            Thing::class,
            $customThing2->thing1
        );

        $this->assertInstanceOf(
            Thing::class,
            $customThing2->thing2
        );
    }
}
