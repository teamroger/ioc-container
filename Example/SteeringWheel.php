<?php declare(strict_types=1);

namespace Example;

class SteeringWheel
{
    public function turnLeft() : void
    {
        echo 'Steering wheel turned left' . "\n";
    }
    
    public function turnRight() : void
    {
        echo 'Steering wheel turned right' . "\n";
    }
}
