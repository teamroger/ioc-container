<?php declare(strict_types=1);

namespace Example;

class Wheel
{
    protected $tyreBrand = null;
    
    public function __construct(TyreBrand $tyreBrand)
    {
        $this->tyreBrand = $tyreBrand;
    }
    
    public function move() : void
    {
        echo 'Wheel with a ' . $this->tyreBrand . ' tyre is moving.' . "\n";
    }
}
