<?php

return [
    'driver' => [
        'name' => 'Michael',
        'age' => 33
    ],

    'car' => [
        'wheels' => [
            'tyres' => [
                'brand' => 'Michellinio'
            ]
        ]
    ],
];
