<?php declare(strict_types=1);

namespace Example;

class TyreBrand
{
    protected $brand = '';
    
    public function __construct(string $brand)
    {
        $this->brand = $brand;
    }
    
    public function __toString() : string
    {
        return $this->brand;
    }
}
