<?php declare(strict_types=1);

namespace Example;

class Car
{
    protected $wheel1 = null;
    protected $wheel2 = null;
    protected $wheel3 = null;
    protected $wheel4 = null;
    protected $steeringWheel = null;
    protected $numberPlate = '';
    
    public function __construct(
        Wheel $wheel1,
        Wheel $wheel2,
        Wheel $wheel3,
        Wheel $wheel4,
        SteeringWheel $steeringWheel
    ) {
        $this->wheel1 = $wheel1;
        $this->wheel2 = $wheel2;
        $this->wheel3 = $wheel3;
        $this->wheel4 = $wheel4;
        $this->steeringWheel = $steeringWheel;
    }
    
    public function setNumberPlate(string $plate) : Car
    {
        $this->numberPlate = $plate;
        
        return $this;
    }
    
    public function getNumberPlate() : string
    {
        return $this->numberPlate;
    }
    
    public function drive() : void
    {
        echo "\n" .
            'Car[' .
            $this->getNumberPlate() .
            '] is attempting to drive.' .
            "\n";
        
        $this->wheel1->move();
        $this->wheel2->move();
        $this->wheel3->move();
        $this->wheel4->move();
        $this->steeringWheel->turnLeft();
        $this->steeringWheel->turnRight();
        
        echo 'Car has finished driving.' . "\n";
    }
}
