<?php declare(strict_types=1);

namespace Example;

class House
{
    protected $number = -1;
    protected $people = [];
    
    public function setHouseNumber(int $number) : House
    {
        $this->number = $number;
        
        return $this;
    }
    
    public function addPerson(Person $person) : House
    {
        $this->people[] = $person;

        return $this;
    }
    
    public function countPeope() : int
    {
        return count($this->people);
    }
}
