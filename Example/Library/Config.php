<?php declare(strict_types=1);

namespace Example\Library;

use Exception;

class Config
{
    protected $settings = [];

    public function __construct(array $settings = [])
    {
        $this->settings = $settings;
    }

    public function get(string $key)
    {
        $parts = explode('.', $key);
        $currentArray = $this->settings;

        foreach ($parts as $part) {
            if (array_key_exists($part, $currentArray)) {
                $currentArray = $currentArray[$part];
            } else {
                throw new Exception('Part "' . $part . '" not found in settings.');
            }
        }

        return $currentArray;
    }
}
