<?php declare(strict_types=1);

namespace Example\Resolvers;

use Example\House;
use IoC\Contracts\Resolver;
use IoC\Contracts\Container;

class FamilyResolver implements Resolver
{
    public function resolve(Container $container) : object
    {
        $house = new House;
        
        $house->setHouseNumber(5);
        
        $house->addPerson($container->resolve('person'));
        $house->addPerson($container->resolve('person'));
        $house->addPerson($container->resolve('person'));
        $house->addPerson($container->resolve('person'));
        
        return $house;
    }
}
