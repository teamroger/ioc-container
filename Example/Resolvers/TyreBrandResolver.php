<?php declare(strict_types=1);

namespace Example\Resolvers;

use Example\TyreBrand;
use IoC\Contracts\Resolver;
use IoC\Contracts\Container;

class TyreBrandResolver implements Resolver
{
    public function resolve(Container $container) : object
    {
        $config = $container->resolve('config');
        $brand = $config->get('car.wheels.tyres.brand');
        $tyreBrand = new TyreBrand($brand);

        return $tyreBrand;
    }
}
