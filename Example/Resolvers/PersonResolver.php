<?php declare(strict_types=1);

namespace Example\Resolvers;

use Example\Person;
use IoC\Contracts\Resolver;
use IoC\Contracts\Container;

class PersonResolver implements Resolver
{
    public function resolve(Container $container) : object
    {
        $config = $container->resolve('config');
        $name = $config->get('driver.name');
        $person = new Person($name);

        return $person;
    }
}
