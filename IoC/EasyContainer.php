<?php declare(strict_types=1);

namespace IoC;

use IoC\Resolvers\Factory;
use IoC\Resolvers\Instance;
use IoC\Resolvers\Singleton;

class EasyContainer extends Container
{
    public function factory(
        string $key,
        string $className,
        array $dependencyKeys = []
    ) : EasyContainer {
        $this->register(
            $key,
            new Factory($className, $dependencyKeys)
        );

        return $this;
    }

    public function singleton(
        string $key,
        string $className,
        array $dependencyKeys = []
    ) : EasyContainer {
        $this->register(
            $key,
            new Singleton(new Factory($className, $dependencyKeys))
        );

        return $this;
    }

    public function custom(
        string $key,
        string $resolverClassName
    ) : EasyContainer {
        $this->register($key, new $resolverClassName);

        return $this;
    }

    public function once(
        string $key,
        string $resolverClassName
    ) : EasyContainer {
        $this->register($key, new Singleton(new $resolverClassName));

        return $this;
    }

    public function instance(string $key, object $instance) : EasyContainer
    {
        $this->register($key, new Instance($instance));

        return $this;
    }
}
