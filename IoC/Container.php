<?php declare(strict_types=1);

namespace IoC;

use IoC\Contracts\Resolver;
use IoC\Exceptions\KeyNotFoundException;
use IoC\Contracts\Container as IContainer;

class Container implements IContainer
{
    protected $resolvers = [];
    
    public function register(string $key, Resolver $resolver) : IContainer
    {
        $this->resolvers[$key] = $resolver;

        return $this;
    }
    
    public function resolve(string $key) : object
    {
        if (array_key_exists($key, $this->resolvers)) {
            return $this->resolvers[$key]->resolve($this);
        }

        $message = 'Key "' . $key . '" was not registered.';
        throw new KeyNotFoundException($message);
    }
}
