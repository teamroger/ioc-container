<?php declare(strict_types=1);

namespace IoC\Resolvers;

use IoC\Contracts\Resolver;
use IoC\Contracts\Container;

class Singleton implements Resolver
{
    private $child;
    private $instance;

    public function __construct(Resolver $child)
    {
        $this->child = $child;
    }

    public function resolve(Container $container) : object
    {
        if ($this->instance === null) {
            $this->instance = $this->child->resolve($container);
        }
        
        return $this->instance;
    }
}
