<?php declare(strict_types=1);

namespace IoC\Resolvers;

use IoC\Contracts\Resolver;
use IoC\Contracts\Container;

class Instance implements Resolver
{
    protected $instance = null;
    
    public function __construct(object $instance)
    {
        $this->instance = $instance;
    }
    
    public function resolve(Container $container) : object
    {
        return $this->instance;
    }
}
