<?php declare(strict_types=1);

namespace IoC\Resolvers;

use ReflectionClass;
use IoC\Contracts\Resolver;
use IoC\Contracts\Container;

class Factory implements Resolver
{
    protected $className = '';
    protected $dependencyNames = [];
    protected $dependencies = [];
    
    public function __construct(string $className, array $dependencyNames = [])
    {
        $this->className = $className;
        $this->dependencyNames = $dependencyNames;
    }
    
    public function resolve(Container $container) : object
    {
        if (count($this->dependencyNames) > 0 && empty($this->dependencies)) {
            $resolve = [$container, 'resolve'];
            $this->dependencies = array_map($resolve, $this->dependencyNames);
        }

        $class = new ReflectionClass($this->className);
        $object = $class->newInstanceArgs($this->dependencies);

        return $object;
    }
}
