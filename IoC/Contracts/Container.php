<?php declare(strict_types=1);

namespace IoC\Contracts;

interface Container
{
    public function register(string $key, Resolver $resolver) : Container;
    public function resolve(string $key) : object;
}
