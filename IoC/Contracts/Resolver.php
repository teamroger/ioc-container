<?php declare(strict_types=1);

namespace IoC\Contracts;

interface Resolver
{
    public function resolve(Container $container) : object;
}
