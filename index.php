<?php

require_once 'vendor/autoload.php';

use Example\Car;
use Example\Wheel;
use IoC\EasyContainer;
use Example\SteeringWheel;
use Example\Library\Config;
use Example\Resolvers\PersonResolver;
use Example\Resolvers\FamilyResolver;
use Example\Resolvers\TyreBrandResolver;

$settings = require_once 'Example/config/settings.php';

$container = new EasyContainer;

$container->instance('config', new Config($settings))
    ->once('tyreBrand', TyreBrandResolver::class)
    ->factory('wheel', Wheel::class, ['tyreBrand'])
    ->singleton('steeringWheel', SteeringWheel::class)
    ->singleton('car', Car::class, ['wheel', 'wheel', 'wheel', 'wheel', 'steeringWheel'])
    ->once('person', PersonResolver::class)
    ->once('house', FamilyResolver::class);

$car = $container->resolve('car');
$car->setNumberPlate('ABC123');

$car->drive();

$sameCar = $container->resolve('car');

$sameCar->drive();

$house1 = $container->resolve('house');
$house2 = $container->resolve('house');

$house2->setHouseNumber(9);
var_dump($house1);
var_dump($house2);
